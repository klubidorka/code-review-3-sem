#include <algorithm>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

const int cAlphabetSize = 26;

class different_substring_num {
public:
    explicit different_substring_num(const std::string &pattern);

    int count_different_substrings();

private:
    int text_length;
    std::string text;
    std::vector<int> permutation;                            // Current permutation of substrings with 2^step length
    std::vector<std::vector<int>> equivalence_class;         // Equivalence means similarity
    std::vector<int> position;                               // Indexes of first element of this equivalence class
    std::vector<bool> string_num;                            // 1 if it is a substring of first string, else 0

    void calculate_step_1();                                 // Calculating arrays in case of one-char suffixes
    void calculate_step_k(int step);                         // Calculating arrays in case of 2^step-length suffixes
    void counting_sort(int step);

    void build_suffix_array();

    int longest_common_prefix(int first_index, int second_index);
};

void different_substring_num::calculate_step_1() {
    equivalence_class[0].resize(text_length);

    for (int i = 0; i < text_length; ++i) {
        equivalence_class[0][i] = static_cast<int>(text[i] - '_');
    }

    counting_sort(0);

    for (int l = 0; l < text_length; ++l) {
        permutation[position[equivalence_class[0][l]]++] = l;
    }
}

void different_substring_num::calculate_step_k(int step) {
    // Permutation and equivalence classes on this step
    std::vector<int > k_permutation;
    std::vector<int > k_equivalence_class;
    k_permutation.resize(text_length);
    k_equivalence_class.resize(text_length + cAlphabetSize);

    // Use the idea of radix sort (LSD)
    for (int i = 0; i < text_length; ++i) {
        permutation[i] = (permutation[i] - (1 << step) + text_length) % text_length;
    }

    counting_sort(step);

    // Calculating new permutation
    for (int l = 0; l < text_length; ++l) {
        k_permutation[position[equivalence_class[step][permutation[l]]]++] = permutation[l];
    }
    permutation = k_permutation;

    // Calculating new equivalence classes
    int current_class = 0;
    k_equivalence_class[k_permutation[0]] = current_class;
    for (int k = 1; k < text_length; ++k) {
        if (equivalence_class[step][permutation[k - 1]] !=
            equivalence_class[step][permutation[k]] ||
            equivalence_class[step][(permutation[k - 1] + (1 << step)) % text_length] !=
            equivalence_class[step][(permutation[k] + (1 << step)) % text_length]) {
            current_class++;
        }
        k_equivalence_class[permutation[k]] = current_class;
    }

    k_equivalence_class.resize(text_length);

    equivalence_class[step + 1] = k_equivalence_class;
}

// Calculating position with counting sort
void different_substring_num::counting_sort(int step) {
    position.resize(0);
    position.resize(text_length + cAlphabetSize, 0);

    // Calculating number of elements in this equivalence class
    for (int j = 0; j < text_length; ++j) {
        position[equivalence_class[step][j]]++;
    }

    // Calculating indexes of first elements of this equivalence class
    int class_beginning_index = 0;
    int number_of_elements_in_current_class = 0;
    for (int k = 0; k < text_length + cAlphabetSize; ++k) {
        number_of_elements_in_current_class = position[k];
        position[k] = class_beginning_index;
        class_beginning_index += number_of_elements_in_current_class;
    }
}

void different_substring_num::build_suffix_array() {
    calculate_step_1();
    for (int k = 0; (1 << k) < text_length; ++k) {
        calculate_step_k(k);
    }
}

int different_substring_num::longest_common_prefix(int first_index, int second_index) {
    int answer = 0;
    for (auto i = static_cast<int>(ceil(log(text_length) / log(2))); i >= 0; --i) {
        if (equivalence_class[i][first_index] == equivalence_class[i][second_index]) {
            answer += 1 << i;
            first_index += 1 << i;
            second_index += 1 << i;
        }
    }
    return answer;
}

different_substring_num::different_substring_num(const std::string &pattern) {
    text = pattern;
    text_length = pattern.size();
    permutation.resize(text_length);
    equivalence_class.resize(static_cast<int>(ceil(log(text_length) / log(2))) + 1);
    position.resize(text_length + cAlphabetSize);
    string_num.resize(text_length);

    build_suffix_array();
}

int different_substring_num::count_different_substrings() {
    int answer = text_length * (text_length - 1) / 2;
    for (int i = 0; i < text_length - 2; ++i) {
        answer -= longest_common_prefix(permutation[i], permutation[i + 1]);
    }
    return answer;
}


int main() {
    std::string pattern;
    std::cin >> pattern;
    pattern += "{";
    different_substring_num s_a(pattern);
    std::cout << s_a.count_different_substrings() << std::endl;
    return 0;
}