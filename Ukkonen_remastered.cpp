#include <cassert>
#include <iostream>
#include <map>
#include <string>
#include <vector>


class suffix_tree {
public:
    struct graph_vertex {
        int parent;
        int left;
        int right;

        graph_vertex(int parent_, int left_, int right_);
    };

private:
    struct node {
        int left;
        int right;
        int parent;
        int link;

        // passage through the symbol
        std::map<char, int> move_with_symbol;

        node();

        node(int left_, int right_, int parent_);

        const int get_length();

        // try to move_with_symbol through symbol c
        int &get(const char &symbol);
    };

    // current symbol
    struct state {
        int vertex;
        int position;

        state(int vertex_, int position_);
    };

    void build_suffix_tree();

    state move_from_state(state current_state, char symbol);

    int add_child(int vertex, int left, int right);

    int split(state current_state);

    int get_link(int vertex);

    state fast_go(int vertex, int left, int right);

    state extend(state current_state, int current_number);

    void DFS(int vertex, std::vector<int> &prev_order, int &new_order);

    std::string first_string;
    std::string pattern;
    std::vector<node> tree;
    std::vector<graph_vertex> graph;
    // where to place new node
    int first_empty_place_index;
public:
    explicit suffix_tree(const std::string &pattern_1, const std::string &pattern_2);

    void construct_graph();

    friend std::ostream &operator<<(std::ostream &os, const suffix_tree &tree);
};


int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    std::string first_string;
    std::string second_string;

    std::cin >> first_string;
    std::cin >> second_string;

    suffix_tree tree(first_string, second_string);

    tree.construct_graph();

    std::cout << tree;

    return 0;
}

suffix_tree::node::node()
        : left(-1),
          right(-1),
          parent(-1),
          link(-1) {
}

suffix_tree::node::node(int left_, int right_, int parent_)
        : left(left_),
          right(right_),
          parent(parent_),
          link(-1) {

}

const int suffix_tree::node::get_length() {
    return right - left;
}

int &suffix_tree::node::get(const char &symbol) {
    if (move_with_symbol.count(symbol) == 0) {
        move_with_symbol[symbol] = -1;
    }
    return move_with_symbol[symbol];
}

suffix_tree::state::state(int vertex_, int position_)
        : vertex(vertex_),
          position(position_) {

}

void suffix_tree::build_suffix_tree() {
    assert(!pattern.empty());
    tree.resize(2 * pattern.size());
    first_empty_place_index = 1;
    tree[0] = node();
    tree[0].link = 0;
    state current_state(0, 0);
    for (size_t i = 0; i < pattern.size(); ++i) {
        current_state = extend(current_state, i);
    }
}

suffix_tree::state suffix_tree::move_from_state(state current_state, char symbol) {
    if (current_state.position == 0) {
        int to = tree[current_state.vertex].get(symbol);
        if (to == -1) {
            return {-1, -1};
        }
        return {to, tree[to].get_length() - 1};
    }
    if (pattern[tree[current_state.vertex].right - current_state.position] != symbol) {
        return {-1, -1};
    }
    return {current_state.vertex, current_state.position - 1};
}

int suffix_tree::add_child(int vertex, int left, int right) {
    int position = first_empty_place_index++;
    tree[vertex].get(pattern[left]) = position;
    tree[position] = node(left, right, vertex);
    return position;
}

int suffix_tree::split(state current_state) {
    int current_vertex = current_state.vertex;
    int up = current_state.position;
    int par = tree[current_vertex].parent;
    int down = tree[current_vertex].get_length() - up;
    if (up == 0) {
        return current_vertex;
    }
    if (down == 0) {
        return par;
    }
    int new_vertex_index = add_child(par, tree[current_vertex].left, tree[current_vertex].right - up);
    tree[current_vertex].left += down;
    tree[current_vertex].parent = new_vertex_index;
    tree[new_vertex_index].get(pattern[tree[current_vertex].left]) = current_vertex;
    return new_vertex_index;
}

int suffix_tree::get_link(int vertex) {
    if (tree[vertex].link != -1) {
        return tree[vertex].link;
    }
    if (vertex == -1) {
        return -1;
    }
    int l = tree[vertex].left;
    if (tree[vertex].parent == 0) {
        l++;
    }
    tree[vertex].link = split(fast_go(get_link(tree[vertex].parent), l, tree[vertex].right));

    return tree[vertex].link;
}

suffix_tree::state suffix_tree::fast_go(int vertex, int left, int right) {
    while (true) {
        if (left == right) {
            return {vertex, 0};
        }
        int to = tree[vertex].get(pattern[left]);
        if (left + tree[to].get_length() > right) {
            return {to, tree[to].get_length() + left - right};
        }
        left += tree[to].get_length();
        vertex = to;
    }
}

suffix_tree::state suffix_tree::extend(state current_state, int current_number) {
    while (true) {
        state destination = move_from_state(current_state, pattern[current_number]);

        if (destination.vertex != -1) {
            return destination;
        }

        int v = split(current_state);
        add_child(v, current_number, static_cast<int>(pattern.size()));
        current_state = state(get_link(v), 0);

        if (v == 0) {
            return current_state;
        }
    }
}

void suffix_tree::DFS(int vertex, std::vector<int> &prev_order, int &new_order) {
    prev_order[vertex] = new_order++;
    for (auto item : tree[vertex].move_with_symbol) {
        DFS(item.second, prev_order, new_order);
    }
}

suffix_tree::graph_vertex::graph_vertex(int parent_, int left_, int right_) :
        parent(parent_),
        left(left_),
        right(right_) {}

suffix_tree::suffix_tree(const std::string &pattern_1, const std::string &pattern_2) :
        pattern(pattern_1 + pattern_2),
        first_string(pattern_1),
        first_empty_place_index(0) {
    build_suffix_tree();
}

void suffix_tree::construct_graph() {
    std::vector<int> prev_order(first_empty_place_index, 0);
    int current_vertex_index = 0;
    DFS(0, prev_order, current_vertex_index);

    graph.assign(static_cast<unsigned int>(first_empty_place_index), graph_vertex(0, 0, 0));
    for (size_t v = 1; v < first_empty_place_index; ++v) {
        graph[prev_order[v]].parent = static_cast<int>(prev_order[tree[v].parent]);
        graph[prev_order[v]].left = tree[v].left;
        graph[prev_order[v]].right = tree[v].right;
    }
}

std::ostream &operator<<(std::ostream &os, const suffix_tree &tree) {
    os << tree.graph.size() << std::endl;
    for (size_t vertex = 1; vertex < tree.graph.size(); ++vertex) {
        int parent = tree.graph[vertex].parent;
        int left = tree.graph[vertex].left;
        int right = tree.graph[vertex].right;
        if (left < tree.first_string.size() && right >= tree.first_string.size()) {
            right = tree.first_string.size();
        }
        if (left < tree.first_string.size()) {
            os << parent << " " << "0 " << left << " " << right << "\n";
        } else {
            left -= tree.first_string.size();
            right -= tree.first_string.size();
            os << parent << " " << "1 " << left << " " << right << "\n";
        }
    }
    return os;
}

