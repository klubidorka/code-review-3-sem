// Алгоритм нахождения лексикографически минимальной строки соответствующей заданной z функции
// https://contest.yandex.ru/contest/9093/run-report/11948872/

#include <iostream>
#include <string>
#include <vector>

// Нахождение строки по префикс функции
std::string prefix_function_to_string(const std::vector<int> &prefix_function) {
    std::string minimal_string;
    if (!prefix_function.empty()){
        minimal_string += "a";
    }
    // Индекс текущего current_min
    int searcher = 0;
    // Наибольший в лексикографическом смысле символ, использовать который уже нельзя
    char current_min = 'a';

    for (int i = 1; i < prefix_function.size(); i++) {
        if (prefix_function[i] == 0) {
            searcher = prefix_function[i - 1];
            current_min = minimal_string[searcher];

            while (searcher != 0) {
                if (current_min < minimal_string[searcher]) {
                    current_min = minimal_string[searcher];
                }
                searcher = prefix_function[searcher - 1];
            }
            if (current_min < minimal_string[searcher]) {
                current_min = minimal_string[searcher];
            }
            minimal_string += ++current_min;
        } else {
            minimal_string += minimal_string[prefix_function[i] - 1];
        }
    }
    return minimal_string;
}

// Преобразование z функции в префикс функцию
std::vector<int> z_function_to_prefix_function(const std::vector<int> &z_function) {
    std::vector<int> prefix_function(z_function.size());

    for (int i = 1; i < z_function.size(); ++i) {
        for (int j = z_function[i] - 1; j >= 0; --j) {
            if (prefix_function[i + j] > 0)
                break;
            prefix_function[j + i] = j + 1;
        }
    }

    return prefix_function;
}

std::string z_function_to_string(const std::vector<int> &z_function) {
    std::vector<int> prefix_function = z_function_to_prefix_function(z_function);
    std::string minimal_string = prefix_function_to_string(prefix_function);
    return minimal_string;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);

    std::vector<int> z_function;
    int number;
    while (std::cin >> number) {
        z_function.push_back(number);
    }

    std::string minimal_string = z_function_to_string(z_function);
    std::cout << minimal_string << std::endl;
    return 0;
}