#include <algorithm>
#include <array>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <queue>

class Endplay {
public:
    int calculate_moves_to_checkmate();

    struct Disposition {
        int KB_x;
        int KB_y;
        int QW_x;
        int QW_y;
        bool is_white_to_play;

        bool check_if_check();

        bool check_if_correct();

        bool check_if_checkmate();

        int code_from_position();

        friend std::istream &operator>>(std::istream &is, Disposition &d);

        Disposition();

    };

private:
    int start_index;
    std::array<bool, 1 << 13> is_visited;
    std::array<int, 1 << 13> moves_to_checkmate;

    static void get_children_or_ancestors(int position_code, std::vector<int> &children, bool is_search_for_children);

    static void get_children(int position_code, std::vector<int> &children);

    static void get_ancestors(int position_code, std::vector<int> &children);

public:
    explicit Endplay(Disposition start_);

    static Disposition position_from_code(int code);
};

int main() {
    Endplay::Disposition d;
    std::cin >> d;
    Endplay e(d);
    std::cout << e.calculate_moves_to_checkmate();
    return 0;
}

int Endplay::calculate_moves_to_checkmate() {
    std::queue<int> positions_being_scanned_indexes;
    for (int i = 0; i < (1 << 13); ++i) {
        Disposition current_position = position_from_code(i);
        if (current_position.check_if_correct() && current_position.check_if_checkmate()) {
            positions_being_scanned_indexes.push(i);
            moves_to_checkmate[i] = 0;
            is_visited[i] = true;
        }
    }
    while (!positions_being_scanned_indexes.empty()) {
        int current_position_index = positions_being_scanned_indexes.front();
        positions_being_scanned_indexes.pop();
        Disposition current_position = position_from_code(current_position_index);

        std::vector<int> ancestors_indexes;
        get_ancestors(current_position_index, ancestors_indexes);

        if (!current_position.is_white_to_play) {
            for (auto it : ancestors_indexes) {
                if (!is_visited[it]) {
                    moves_to_checkmate[it] = moves_to_checkmate[current_position_index] + 1;
                    positions_being_scanned_indexes.push(it);
                    is_visited[it] = true;
                    if (it == start_index) {
                        return moves_to_checkmate[start_index];
                    }
                }
            }
        } else {
            for (auto it : ancestors_indexes) {
                if (!is_visited[it]) {
                    int moves_to_checkmate_ = -1;
                    bool ready_to_be_pushed = true;
                    std::vector<int> children_indexes;
                    get_children(it, children_indexes);
                    for (auto child_index : children_indexes) {
                        if (is_visited[child_index]) {
                            moves_to_checkmate_ = std::max(moves_to_checkmate_, moves_to_checkmate[child_index] + 1);
                        } else {
                            ready_to_be_pushed = false;
                            break;
                        }
                    }
                    if (ready_to_be_pushed) {
                        moves_to_checkmate[it] = moves_to_checkmate_;
                        positions_being_scanned_indexes.push(it);
                        is_visited[it] = true;
                    }
                }
            }
        }
    }
    return moves_to_checkmate[start_index];
}

bool Endplay::Disposition::check_if_check() {
    if (QW_x == KB_x) {
        if (QW_x != 3)
            return true;
        return (KB_y < 3 && QW_y < 3) || (KB_y > 3 && QW_y > 3);
    }
    if (QW_y == KB_y) {
        if (QW_y != 3)
            return true;
        return (KB_x < 3 && QW_x < 3) || (KB_x > 3 && QW_x > 3);
    }

    if (QW_x - KB_x == QW_y - KB_y) {
        if (QW_x - 3 != QW_y - 3)
            return true;
        return ((QW_y > 3 && KB_y > 3) || (QW_y < 3 && KB_y < 3));
    }

    if (QW_x - KB_x == KB_y - QW_y) {
        if (QW_x - 3 != 3 - QW_y)
            return true;
        return ((QW_x > 3 && KB_x > 3) || (QW_x < 3 && KB_x < 3));
    }
    return false;
}

bool Endplay::Disposition::check_if_correct() {
    if (QW_x > 8 || QW_y > 8 || KB_x > 8 || KB_y > 8)
        return false;
    if (QW_x < 1 || QW_y < 1 || KB_x < 1 || KB_y < 1)
        return false;
    if (KB_y < 5 && KB_y > 1 && KB_x < 5 && KB_x > 1)
        return false;
    if (QW_y == KB_y && QW_x == KB_x)
        return false;
    if (QW_y == 3 && QW_x == 3)
        return false;
    if (std::max(abs(QW_y - KB_y), abs(QW_x - KB_x)) == 1) {
        if (is_white_to_play)
            return false;
        if (std::max(abs(QW_y - 3), abs(QW_x - 3)) != 1)
            return false;
    }
    if (is_white_to_play) {
        if (check_if_check()) {
            return false;
        }
    }
    return true;
}

bool Endplay::Disposition::check_if_checkmate() {
    if (is_white_to_play || !check_if_check()) {
        return false;
    }
    std::vector<int> positions;
    get_children(code_from_position(), positions);
    return (positions.empty());
}

int Endplay::Disposition::code_from_position() {
    int position_number = (KB_x - 1) + ((KB_y - 1) << 3) + ((QW_x - 1) << 6) + ((QW_y - 1) << 9);
    return is_white_to_play ? position_number : position_number + 4096;
}

void Endplay::get_children_or_ancestors(int position_code, std::vector<int> &children, bool is_search_for_children) {
    Disposition position = position_from_code(position_code);
    if ((position.is_white_to_play && is_search_for_children) ||
        (!position.is_white_to_play && !is_search_for_children)) {
        position.is_white_to_play = !position.is_white_to_play;
        for (int i = -7; i < 8; ++i) {
            if (i != 0) {
                std::array<Disposition, 4> possible_positions;
                possible_positions.fill(position);

                possible_positions[0].QW_x += i;
                possible_positions[1].QW_y += i;
                possible_positions[2].QW_x += i;
                possible_positions[2].QW_y += i;
                possible_positions[3].QW_x += i;
                possible_positions[3].QW_y -= i;

                for (auto item : possible_positions) {
                    if (item.check_if_correct()) {
                        double first_distance = hypot(abs(position.QW_x - item.QW_x),
                                                      abs(position.QW_y - item.QW_y));
                        double second_distance = hypot(abs(position.QW_x - 3),
                                                       abs(position.QW_y - 3)) +
                                                 hypot(abs(item.QW_x - 3),
                                                       abs(item.QW_y - 3));
                        if (fabs(first_distance - second_distance) > 1e-4) {
                            children.push_back(item.code_from_position());
                        }
                    }
                }

            }
        }
    } else {
        position.is_white_to_play = !position.is_white_to_play;
        for (int i = -1; i < 2; ++i) {
            for (int j = -1; j < 2; ++j) {
                if (i != 0 || j != 0) {
                    Disposition current_position = position;
                    current_position.KB_x += i;
                    current_position.KB_y += j;
                    if (current_position.check_if_correct()) {
                        children.push_back(current_position.code_from_position());
                    }
                }
            }
        }
    }
}

void Endplay::get_children(int position_code, std::vector<int> &children) {
    get_children_or_ancestors(position_code, children, true);
}

void Endplay::get_ancestors(int position_code, std::vector<int> &children) {
    get_children_or_ancestors(position_code, children, false);
}

Endplay::Endplay(Disposition start_) : start_index(start_.code_from_position()) {
    is_visited.fill(false);
}

Endplay::Disposition Endplay::position_from_code(int code) {
    Endplay::Disposition current_position;
    if (code >= 4096) {
        code -= 4096;
        current_position.is_white_to_play = false;
    } else {
        current_position.is_white_to_play = true;
    }
    current_position.QW_y = (code - code % (1 << 9)) / (1 << 9) + 1;
    code = code % (1 << 9);
    current_position.QW_x = (code - code % (1 << 6)) / (1 << 6) + 1;
    code = code % (1 << 6);
    current_position.KB_y = (code - code % (1 << 3)) / (1 << 3) + 1;
    code = code % (1 << 3);
    current_position.KB_x = code + 1;
    return current_position;
}

std::istream &operator>>(std::istream &is, Endplay::Disposition &d) {
    std::string QW, KB;
    is >> QW >> KB;
    d.QW_x = QW[0] - 96;
    d.QW_y = QW[1] - 48;
    d.KB_x = KB[0] - 96;
    d.KB_y = KB[1] - 48;
    return is;
}

Endplay::Disposition::Disposition() : KB_x(0), KB_y(0), QW_x(0), QW_y(0), is_white_to_play(true) {}
