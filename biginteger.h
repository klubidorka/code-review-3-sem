#include <iostream>
#include <string>
#include <vector>


class BigInteger {
public:

    BigInteger();

    BigInteger(int given_value);

    BigInteger(const BigInteger &other);

    BigInteger &operator=(const BigInteger &other);


    bool operator==(const BigInteger &other) const;

    bool operator!=(const BigInteger &other) const;

    bool operator>=(const BigInteger &other) const;

    bool operator>(const BigInteger &other) const;

    bool operator<(const BigInteger &other) const;

    bool operator<=(const BigInteger &other) const;


    BigInteger operator-() const;

    BigInteger &operator+=(const BigInteger &other);

    BigInteger &operator-=(const BigInteger &other);

    BigInteger &operator*=(const BigInteger &other);

    BigInteger &operator/=(const BigInteger &other);

    BigInteger operator%=(const BigInteger &other);

    BigInteger &operator++();

    BigInteger operator++(int);

    BigInteger &operator--();

    BigInteger operator--(int);


    friend BigInteger operator*(const BigInteger &first, const BigInteger &second);

    friend BigInteger operator-(const BigInteger &first, const BigInteger &second);

    friend BigInteger operator+(const BigInteger &first, const BigInteger &second);

    friend BigInteger operator/(const BigInteger &first, const BigInteger &second);

    friend BigInteger operator%(const BigInteger &first, const BigInteger &second);


    friend std::istream &operator>>(std::istream &is, BigInteger &value);

    friend std::ostream &operator<<(std::ostream &os, const BigInteger &value);


    BigInteger abs() const;

    explicit operator bool() const;

    std::string toString() const;

private:
    std::vector<int> digits;
    bool is_positive;

    void deleteLeadingZeros();
};

BigInteger operator+(const BigInteger &first, const BigInteger &second) {
    BigInteger buff(first);
    buff += second;
    return buff;
}

BigInteger operator-(const BigInteger &first, const BigInteger &second) {
    BigInteger buff(first);
    buff -= second;
    return buff;
}

BigInteger operator*(const BigInteger &first, const BigInteger &second) {
    BigInteger buff = first;
    buff *= second;
    return buff;
}

BigInteger operator/(const BigInteger &first, const BigInteger &second) {
    BigInteger buff = first;
    buff /= second;
    return buff;
}

BigInteger operator%(const BigInteger &first, const BigInteger &second) {
    BigInteger temp = first;
    temp %= second;
    return temp;
}

std::istream &operator>>(std::istream &is, BigInteger &value) {
    std::string str;
    value.digits.clear();
    is >> str;
    if (str == "0" || str == "-0" || str.empty()) {
        value.digits.reserve(1);
        value.digits.clear();
        value.digits.push_back(0);
        value.is_positive = true;
        return is;
    }
    int isNegative = 0;
    if (str[0] == '-') {
        value.is_positive = false;
        ++isNegative;
    } else {
        value.is_positive = true;
    }
    value.digits.reserve(str.length() - isNegative);
    for (int i = str.length() - 1; i >= isNegative; --i) {
        value.digits.push_back(static_cast<int>(str[i]) - '0');
    }
    return is;
}

std::ostream &operator<<(std::ostream &os, const BigInteger &value) {
    if (value.digits.empty()) {
        os << 0;
        return os;
    }
    if (!value.is_positive)
        os << "-";
    for (int i = value.digits.size() - 1; i >= 0; --i) {
        os << value.digits[i];
    }
    return os;
}

BigInteger BigInteger::abs() const {
    BigInteger result(*this);
    result.is_positive = true;
    return result;
}

BigInteger::BigInteger() : is_positive(true) {
    digits.emplace_back(0);
}

BigInteger::BigInteger(const int given_value) {
    int value_copy = given_value;
    if (value_copy >= 0)
        is_positive = true;
    else {
        is_positive = false;
        value_copy = -value_copy;
    }
    if (value_copy == 0) {
        digits.push_back(0);
    }
    while (value_copy > 0) {
        digits.push_back(value_copy % 10);
        value_copy /= 10;
    }
}

BigInteger::BigInteger(const BigInteger &other) {
    is_positive = other.is_positive;
    for (int i = 0; i < digits.size(); i++) {
        digits.push_back(other.digits[i]);
    }
}

bool BigInteger::operator==(const BigInteger &other) const {
    if (digits.size() != other.digits.size() || is_positive != other.is_positive)
        return false;
    for (int i = 0; i < digits.size(); i++) {
        if (digits[i] != other.digits[i])
            return false;
    }
    return true;
}

BigInteger &BigInteger::operator=(const BigInteger &other) {
    if (this == &other)
        return *this;
    digits = other.digits;
    is_positive = other.is_positive;
    return *this;
}

bool BigInteger::operator!=(const BigInteger &other) const {
    return !(*this == other);
}

bool BigInteger::operator>=(const BigInteger &other) const {
    if (*this == other)
        return true;

    if (is_positive && !other.is_positive)
        return true;

    if (!is_positive && other.is_positive)
        return false;

    if (digits.size() > other.digits.size()) {
        return is_positive;

    }
    if (digits.size() < other.digits.size()) {
        return !is_positive;
    }
    for (int i = digits.size() - 1; i >= 0; --i) {
        if (digits[i] > other.digits[i]) {
            return is_positive;
        }
        if (digits[i] < other.digits[i]) {
            return !is_positive;
        }
    }
    return true;
}

bool BigInteger::operator>(const BigInteger &other) const {
    return *this >= other && *this != other;
}

bool BigInteger::operator<(const BigInteger &other) const {
    return !(*this >= other);
}

bool BigInteger::operator<=(const BigInteger &other) const {
    return *this < other || *this == other;
}

BigInteger BigInteger::operator-() const {
    BigInteger temp(*this);
    if (digits.size() == 1 && digits[0] == 0)
        return temp;
    else {
        temp.is_positive = !temp.is_positive;
        return temp;
    }
}

std::string BigInteger::toString() const {
    std::string str;
    if (!is_positive) {
        str += "-";
    }
    for (int i = digits.size() - 1; i >= 0; --i) {
        auto symbol = static_cast<char>(digits[i] + '0');
        str += symbol;
    }
    return str;
}

void BigInteger::deleteLeadingZeros() {
    while (!digits.empty() && digits[digits.size() - 1] == 0) {
        digits.pop_back();
        if (digits.size() == 1 && digits[0] == 0)
            break;
    }
}

BigInteger &BigInteger::operator+=(const BigInteger &other) {
    if (is_positive && !other.is_positive)
        return *this -= other.abs();
    if (!is_positive && other.is_positive) {
        BigInteger temp(*this);
        *this = other;
        return *this += temp;
    }
    int summSize = std::max(digits.size(), other.digits.size());

    BigInteger summ;
    summ.digits.clear();
    int overflow = 0;

    for (int i = 0; i < summSize; i++) {
        if (i <= std::min(digits.size() - 1, other.digits.size() - 1)) {
            summ.digits.push_back(digits[i] + other.digits[i]);
        } else {
            if (digits.size() >= other.digits.size()) {
                summ.digits.push_back(digits[i]);
            } else
                summ.digits.push_back(other.digits[i]);
        }
        if (static_cast<bool>(overflow)) {
            summ.digits[i] += overflow;
            overflow = 0;
        }
        overflow = summ.digits[i] / 10;
        summ.digits[i] %= 10;
    }

    if (static_cast<bool>(overflow)) {
        summ.digits.push_back(overflow);
        overflow %= 10;
    }

    summ.is_positive = is_positive;
    *this = summ;
    deleteLeadingZeros();
    return *this;
}

BigInteger &BigInteger::operator-=(const BigInteger &other) {
    BigInteger other_copy = other;
    if (is_positive && !other_copy.is_positive)
        return *this += other_copy.abs();
    if (!is_positive && other_copy.is_positive) {
        is_positive = true;
        *this += other_copy;
        is_positive = false;
        return *this;
    }
    BigInteger subtrahend;
    subtrahend.digits.clear();
    if (*this == other_copy) {
        subtrahend.digits.push_back(0);
        is_positive = true;
        digits.clear();
        *this = subtrahend;
        return *this;
    }
    if (!is_positive && !other_copy.is_positive) {
        BigInteger temp = *this;
        *this = other_copy.abs();
        *this -= temp.abs();
        return *this;
    }

    BigInteger *smaller_one;
    BigInteger *greater_one;

    if (*this > other_copy) {
        subtrahend.is_positive = true;
        smaller_one = &other_copy;
        greater_one = this;
    } else {
        subtrahend.is_positive = false;
        smaller_one = this;
        greater_one = &other_copy;
    }

    for (int i = 0; i < smaller_one->digits.size(); i++) {
        if (smaller_one->digits[i] > greater_one->digits[i]) {
            greater_one->digits[i + 1]--;
            greater_one->digits[i] += 10;
        }
        subtrahend.digits.push_back(greater_one->digits[i] - smaller_one->digits[i]);
    }
    int last_elem_index = smaller_one->digits.size() - 1;
    if (static_cast<int>(smaller_one->digits.size()) > smaller_one->digits.size() &&
        greater_one->digits[last_elem_index] < smaller_one->digits[last_elem_index]) {
        subtrahend.digits.push_back(
                greater_one->digits[last_elem_index] - smaller_one->digits[last_elem_index]);
    }

    if (smaller_one->digits.size() < greater_one->digits.size()) {
        for (int i = smaller_one->digits.size(); i < greater_one->digits.size() - 1; i++) {
            if (i != greater_one->digits.size() - 1 && 0 > greater_one->digits[i]) {
                greater_one->digits[i + 1]--;
                greater_one->digits[i] += 10;
            }
            subtrahend.digits.push_back(greater_one->digits[i]);
        }
        subtrahend.digits.push_back(greater_one->digits[greater_one->digits.size() - 1]);
    }

    *this = subtrahend;
    deleteLeadingZeros();
    return *this;
}

BigInteger &BigInteger::operator*=(const BigInteger &other) {
    BigInteger result;
    result.is_positive = (is_positive == other.is_positive);
    result.digits.clear();
    result.digits = std::vector<int>(result.digits.size(), 0);

    for (int i = 0; i < digits.size(); i++) {
        int overflow = 0;
        for (int j = 0; j < other.digits.size() || overflow; j++) {
            if (j < other.digits.size()) {
                result.digits[i + j] += digits[i] * other.digits[j] + overflow;
            } else {
                result.digits[i + j] += overflow;
            }
            overflow = result.digits[i + j] / 10;
            result.digits[i + j] -= overflow * 10;
        }
    }
    int position = digits.size() + other.digits.size();
    while (position > 0 && !result.digits[position])
        position--;
    *this = result;
    deleteLeadingZeros();
    return *this;
}

BigInteger &BigInteger::operator/=(const BigInteger &other) {
    BigInteger result;
    result.is_positive = (other.is_positive == is_positive);
    result.digits = std::vector<int>(result.digits.size(), 0);
    BigInteger currentValue;
    for (int i = digits.size() - 1; i >= 0; --i) {
        currentValue = currentValue * 10 + digits[i];
        int currentNum = 0;
        int left = 0;
        int right = 10;

        while (left <= right) {
            const int mid = (left + right) / 2;
            BigInteger current = other.abs() * mid;
            if (current <= currentValue) {
                currentNum = mid;
                left = mid + 1;
            } else
                right = mid - 1;
        }
        result.digits[i] = currentNum;
        currentValue = currentValue - other.abs() * currentNum;
    }
    result.deleteLeadingZeros();
    *this = result;
    deleteLeadingZeros();
    return *this;
}

BigInteger BigInteger::operator%=(const BigInteger &other) {
    *this = (*this - ((*this / other) * other));
    return *this;
}

BigInteger &BigInteger::operator++() {
    return (*this += 1);
}

BigInteger BigInteger::operator++(int) {
    BigInteger temp = *this;
    ++*this;
    return temp;
}

BigInteger &BigInteger::operator--() {
    return (*this -= 1);
}

BigInteger BigInteger::operator--(int) {
    BigInteger temp = *this;
    --*this;
    return temp;
}

BigInteger::operator bool() const {
    return *this != 0;
}