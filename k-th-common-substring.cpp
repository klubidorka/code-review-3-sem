#include <algorithm>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

// English alphabet and two screen off symbols
const int cAlphabetSize = 28;

class suffix_array {
public:
    suffix_array(const std::string &pattern, int first_size);

    void build_suffix_array();

    std::pair<bool, std::string> k_th_common_substring(long long k);

private:
    std::string text;
    std::vector<int> permutation;                               // Current permutation of substrings with 2^step length
    std::vector<std::vector<int>> equivalence_class;            // Equivalence means similarity
    std::vector<int> position;                                  // Indexes of first element of this equivalence class
    std::vector<bool> string_num;                               // 1 if it is a substring of first string, else 0
    size_t first_string_length;
    size_t string_length;

private:
    void calculate_step_1();                                    // Calculating arrays in case of one-char suffixes
    void calculate_step_k(int step);                            // Calculating arrays in case of 2^step-length suffixes
    void counting_sort(int step);

    void calculate_string_num();

    int longest_common_prefix(int first_index, int second_index);
};

void suffix_array::calculate_step_1() {
    equivalence_class[0].resize(string_length);

    for (int i = 0; i < string_length; ++i) {
        equivalence_class[0][i] = static_cast<int>(text[i] - '_');
    }

    counting_sort(0);

    for (int l = 0; l < string_length; ++l) {
        permutation[position[equivalence_class[0][l]]++] = l;
    }
}

void suffix_array::calculate_step_k(int step) {
    // Permutation and equivalence classes on this step
    std::vector<int> k_permutation, k_equivalence_class;
    k_permutation.resize(string_length);
    k_equivalence_class.resize(string_length + cAlphabetSize);

    // Use the idea of radix sort (LSD)
    for (int i = 0; i < string_length; ++i) {
        permutation[i] = (permutation[i] - (1 << step) + string_length) % string_length;
    }

    counting_sort(step);

    // Calculating new permutation
    for (int l = 0; l < string_length; ++l) {
        k_permutation[position[equivalence_class[step][permutation[l]]]++] = permutation[l];
    }
    permutation = k_permutation;

    // Calculating new equivalence classes
    unsigned int current_class = 0;
    k_equivalence_class[k_permutation[0]] = current_class;
    for (int k = 1; k < string_length; ++k) {
        if (equivalence_class[step][permutation[k - 1]] !=
            equivalence_class[step][permutation[k]] ||
            equivalence_class[step][(permutation[k - 1] + (1 << step)) % string_length] !=
            equivalence_class[step][(permutation[k] + (1 << step)) % string_length]) {
            current_class++;
        }
        k_equivalence_class[permutation[k]] = current_class;
    }

    k_equivalence_class.resize(string_length);

    equivalence_class[step + 1] = k_equivalence_class;
}

// Calculating position with counting sort
void suffix_array::counting_sort(int step) {
    position.resize(0);
    position.resize(string_length + cAlphabetSize, 0);

    // Calculating number of elements in this equivalence class
    for (int j = 0; j < string_length; ++j) {
        position[equivalence_class[step][j]]++;
    }

    // Calculating indexes of first elements of this equivalence class
    unsigned int class_beginning_index = 0;
    unsigned int number_of_elements_in_current_class = 0;
    for (int k = 0; k < string_length + cAlphabetSize; ++k) {
        number_of_elements_in_current_class = position[k];
        position[k] = class_beginning_index;
        class_beginning_index += number_of_elements_in_current_class;
    }
}

void suffix_array::calculate_string_num() {
    for (const auto &it : permutation) {
        it < first_string_length ? string_num[it] = true : string_num[it] = false;
    }
}

int suffix_array::longest_common_prefix(int first_index, int second_index) {
    unsigned int answer = 0;
    for (auto i = static_cast<int>(ceil(log(string_length) / log(2))); i >= 0; --i) {
        if (equivalence_class[i][first_index] == equivalence_class[i][second_index]) {
            answer += 1 << i;
            first_index += 1 << i;
            second_index += 1 << i;
        }
    }
    return answer;
}

suffix_array::suffix_array(const std::string &pattern, int first_size) {
    text = pattern;
    string_length = pattern.size();
    permutation.resize(string_length);
    equivalence_class.resize(static_cast<size_t>(ceil(log(string_length) / log(2))) + 1);
    position.resize(string_length + cAlphabetSize);
    string_num.resize(string_length);
    first_string_length = first_size;

    build_suffix_array();
}

void suffix_array::build_suffix_array() {
    calculate_step_1();
    for (size_t k = 0; (1 << k) < string_length; ++k) {
        calculate_step_k(k);
    }
}

std::pair<bool, std::string> suffix_array::k_th_common_substring(long long k) {
    calculate_string_num();

    // Substring that needs to be found
    std::string answer;

    // Find first pair of substrings that belong to different strings
    size_t i = 0;
    while (string_num[permutation[i]] == string_num[permutation[i + 1]]) {
        i++;
    }

    unsigned long counter = 0;       // Number of found common substrings
    long long min_lcp = 0;           // Lowest LCP on the interval

    while (i < permutation.size() - 1 && counter < k) {
        unsigned int current_lcp = longest_common_prefix(permutation[i], permutation[i + 1]);

        answer = text.substr(permutation[i], current_lcp);

        // Increase counter by the number of found common substrings

        if (current_lcp > min_lcp) {
            counter += current_lcp - min_lcp;
        }

        min_lcp = current_lcp;
        ++i;

        // if substring is found
        if (counter == k) {
            break;
        }

        while (i + 1 < permutation.size() && string_num[permutation[i]] == string_num[permutation[i + 1]]) {
            current_lcp = longest_common_prefix(permutation[i], permutation[i + 1]);
            if (current_lcp < min_lcp) {
                min_lcp = current_lcp;
            }
            i++;
        }
    }

    if (i == permutation.size() - 1 && answer.size() < k) {
        return std::make_pair(false, "");
    }
    if (counter > k) {
        while (counter != k) {
            answer.pop_back();
            counter--;
        }
    }
    return std::make_pair(true, answer);
}

int main() {
    std::string s, t;
    std::cin >> s >> t;

    long long k;
    std::cin >> k;

    suffix_array suff_arr(s + "`" + t + "_", s.size());
    if (suff_arr.k_th_common_substring(k).first) {
        std::cout << suff_arr.k_th_common_substring(k).second << std::endl;
    } else {
        std::cout << "-1" << std::endl;
    }
    return 0;
}
