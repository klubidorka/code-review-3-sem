#include <array>
#include <iostream>
#include <vector>

const size_t ALPHABET_SIZE = 26;

class masked_patterns_finder {
public:
    explicit masked_patterns_finder(const std::string &mask, char mask_symbol_);

    std::vector<int> text_processing(const std::string &text);

    struct Vertex {
        Vertex(int parent, char parent_symbol);

        // Символ, по которому перешли в вершину
        const char symbol;
        const int parent;
        int suffix_link;
        bool is_terminal;

        // Сыновья вершины
        std::array<int, ALPHABET_SIZE> edges;
        // Переходы  из данной вершины
        std::array<int, ALPHABET_SIZE> fast_move;
        // Номера шаблонов, с которыми связана данная вершина
        std::vector<int> patterns_positions;
    };

private:
    void add_pattern(const std::pair<int, int> &submask_position, int pattern_index);

    void find_submasks(const std::string &mask);

    int get_suffix_link(int index);

    int get_link(int index, char symbol);

private:
    std::vector<Vertex> automation_vertices;
    std::string mask;
    const char mask_symbol;
    const int pattern_size;
    std::vector<std::pair<int, int>> submask_coordinates;
};

int main() {
    std::string pattern;
    std::string text;

    std::cin >> pattern;
    std::cin >> text;

    masked_patterns_finder finder(pattern, '?');
    std::vector<int> positions = finder.text_processing(text);

    for (auto it : positions) {
        std::cout << it << " ";
    }

    std::cout << std::endl;
    return 0;
}

masked_patterns_finder::masked_patterns_finder(const std::string &mask, char mask_symbol_)
        : automation_vertices(1, Vertex(0, -1)),
          mask(mask),
          mask_symbol(mask_symbol_),
          pattern_size(mask.size()) {
    automation_vertices[0].suffix_link = 0;
    find_submasks(mask);
    for (auto i = 0; i < submask_coordinates.size(); i++) {
        add_pattern(submask_coordinates[i], i);
    }
}

std::vector<int> masked_patterns_finder::text_processing(const std::string &text) {
    std::vector<int> inclusions;
    std::vector<int> result;
    int vertex_index = 0, letter_index = 0;
    // Ищем вхождения всех подшаблонов и увеличиваем счетчик detected_inclusions в индексе,
    // соответвующем началу маски.

    for (auto letter : text) {
        inclusions.push_back(0);
        vertex_index = get_link(vertex_index, letter - 'a');
        int current_vertex_index = vertex_index;
        do {
            if (automation_vertices[current_vertex_index].is_terminal) {
                for (auto it : automation_vertices[current_vertex_index].patterns_positions) {
                    int start_index = letter_index - submask_coordinates[it].second
                                      + submask_coordinates[it].first;
                    // Проверяем, что шаблон вписывается по длине в текст
                    if (start_index - submask_coordinates[it].first >= 0) {
                        inclusions[start_index -
                                   submask_coordinates[it].first]++;
                    }
                }
            }
            current_vertex_index = get_suffix_link(current_vertex_index);

        } while (current_vertex_index != 0);
        letter_index++;
    }
    // Если индекс равен количеству подшаблонов, он является ответом
    for (auto i = 0; i < inclusions.size(); ++i) {
        if (inclusions[i] == submask_coordinates.size() && i + pattern_size <= letter_index) {
            result.push_back(i);
        }
    }
    return result;
}

masked_patterns_finder::Vertex::Vertex(int parent, char parent_symbol) : symbol(parent_symbol),
                                                                         parent(parent),
                                                                         suffix_link(-1),
                                                                         is_terminal(false) {
    edges.fill(-1);
    fast_move.fill(-1);
}

void masked_patterns_finder::add_pattern(const std::pair<int, int> &submask_position, int pattern_index) {
    int current_vertex = 0;
    for (auto i = submask_position.first; i <= submask_position.second; i++) {
        char symbol = mask[i] - 'a';
        if (automation_vertices[current_vertex].edges[symbol] == -1) {
            automation_vertices.emplace_back(Vertex(current_vertex, symbol));
            automation_vertices[current_vertex].edges[symbol] = automation_vertices.size() - 1;
        }
        current_vertex = automation_vertices[current_vertex].edges[symbol];
    }
    automation_vertices[current_vertex].is_terminal = true;
    automation_vertices[current_vertex].patterns_positions.push_back(pattern_index);
}

void masked_patterns_finder::find_submasks(const std::string &mask) {
    std::pair<int, int> submask_position;
    if (isalpha(mask[0])) {
        submask_position.first = 0;
    }
    if (mask[1] == mask_symbol && isalpha(mask[0])) {
        submask_position.second = 0;
        submask_coordinates.push_back(submask_position);
    }

    for (auto i = 1; i < mask.length() - 1; i++) {
        if (mask[i - 1] == mask_symbol && isalpha(mask[i])) {
            submask_position.first = i;
        }
        if (mask[i + 1] == mask_symbol && isalpha(mask[i])) {
            submask_position.second = i;
            submask_coordinates.push_back(submask_position);
        }
    }

    if (mask[mask.length() - 2] == mask_symbol && isalpha(mask[mask.size() - 1])) {
        submask_position.first = mask.size();
    }
    if (isalpha(mask[mask.size() - 1])) {
        submask_position.second = mask.size() - 1;
        submask_coordinates.push_back(submask_position);
    }
}

int masked_patterns_finder::get_suffix_link(int index) {
    if (automation_vertices[index].suffix_link == -1) {
        if (automation_vertices[index].parent == 0) {
            // Если ссылка не определена и вершина - сын корня - задаем ссылку, как сслылку на корень
            automation_vertices[index].suffix_link = 0;
        } else {
            // Иначе ищем ее рекурсивно
            automation_vertices[index].suffix_link =
                    get_link(get_suffix_link(automation_vertices[index].parent), automation_vertices[index].symbol);
        }
    }
    return automation_vertices[index].suffix_link;
}

int masked_patterns_finder::get_link(int index, char symbol) {
    if (automation_vertices[index].fast_move[symbol] == -1) {
        if (automation_vertices[index].edges[symbol] != -1) {
            automation_vertices[index].fast_move[symbol] = automation_vertices[index].edges[symbol];
        } else if (index == 0) {
            automation_vertices[index].fast_move[symbol] = 0;
        } else {
            automation_vertices[index].fast_move[symbol] = get_link(get_suffix_link(index), symbol);
        }
    }
    return automation_vertices[index].fast_move[symbol];
}
