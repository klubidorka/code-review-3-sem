#include <algorithm>
#include <array>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <valarray>
#include <vector>

struct polygon;

struct radius_vector_R2 {
    double x;
    double y;

    radius_vector_R2();

    radius_vector_R2(double x_, double y_);

    radius_vector_R2 operator+(const radius_vector_R2 &second_dot);

    radius_vector_R2 operator-(const radius_vector_R2 &second_dot);

    radius_vector_R2 operator/(const double &divider);

    radius_vector_R2 operator*(const double &multiplier);

    const bool operator<(const radius_vector_R2 &other);

    bool check_if_point_appertains_polygon(polygon &polygon);
};

std::istream &operator>>(std::istream &is, radius_vector_R2 &rad_vec);

struct polygon {
    // Dots sre given in counterclockwise order. first one is lowest and right-most.
    std::vector<radius_vector_R2> polygon_dots;
    int size;

    double get_angle(size_t index);

    void reverse();

    polygon();
};

std::istream &operator>>(std::istream &is, polygon &p);

double count_triangle_area(const radius_vector_R2 &vector_1, const radius_vector_R2 &vector_2);

polygon calculate_Minkowski_sum(polygon &first_polygon, polygon &second_polygon);

bool check_if_polygons_intersect(polygon &polygon_1, polygon &polygon_2);

int main() {
    polygon first_polygon;
    polygon second_polygon;
    std::cin >> first_polygon >> second_polygon;

    if (check_if_polygons_intersect(first_polygon, second_polygon)) {
        std::cout << "YES" << std::endl;
    } else {
        std::cout << "NO" << std::endl;
    }
    return 0;
}

radius_vector_R2::radius_vector_R2() : x(0), y(0) {}

radius_vector_R2::radius_vector_R2(double x_, double y_) : x(x_), y(y_) {}

radius_vector_R2 radius_vector_R2::operator+(const radius_vector_R2 &second_dot) {
    return {x + second_dot.x,
            y + second_dot.y
    };
}

radius_vector_R2 radius_vector_R2::operator-(const radius_vector_R2 &second_dot) {
    return {x - second_dot.x,
            y - second_dot.y
    };
}

radius_vector_R2 radius_vector_R2::operator/(const double &divider) {
    x /= divider;
    y /= divider;
    return *this;
}

radius_vector_R2 radius_vector_R2::operator*(const double &multiplier) {
    x *= multiplier;
    y *= multiplier;
    return *this;
}

const bool radius_vector_R2::operator<(const radius_vector_R2 &other) {
    return (y > other.y || (y == other.y && x < other.x));
}

bool radius_vector_R2::check_if_point_appertains_polygon(polygon &polygon) {
    double space_1 = 0.0;
    double space_2 = 0.0;

    for (auto i = 0; i < polygon.size; ++i) {
        space_1 += count_triangle_area(polygon.polygon_dots[i] - *this,
                                       polygon.polygon_dots[(i + 1) % polygon.size] - *this);
    }
    for (auto i = 0; i < polygon.size; ++i) {
        space_2 += count_triangle_area(polygon.polygon_dots[i] - polygon.polygon_dots[0],
                                       polygon.polygon_dots[(i + 1) % polygon.size] - polygon.polygon_dots[0]);
    }
    return fabs(space_1 - space_2) < 1e-7;
}

std::istream &operator>>(std::istream &is, radius_vector_R2 &rad_vec) {
    is >> rad_vec.x;
    is >> rad_vec.y;
    return is;
}

double polygon::get_angle(size_t index) {
    return atan2(
            polygon_dots[index + 1].y - polygon_dots[index].y,
            polygon_dots[index + 1].x - polygon_dots[index].x);
}

void polygon::reverse() {
    for (size_t i = 0; i < size; ++i) {
        polygon_dots[i] = polygon_dots[i] * -1;
    }
}

polygon::polygon() : size(0) {}

std::istream &operator>>(std::istream &is, polygon &p) {
    is >> p.size;
    radius_vector_R2 dot;
    for (auto i = 0; i < p.size; ++i) {
        is >> dot;
        p.polygon_dots.push_back(dot);
    }
    return is;
}

double count_triangle_area(const radius_vector_R2 &vector_1, const radius_vector_R2 &vector_2) {
    return fabs(vector_1.x * vector_2.y - vector_1.y * vector_2.x) / 2;
}

void regroup_dots(polygon &polygon_) {
    std::reverse(polygon_.polygon_dots.begin(), polygon_.polygon_dots.end());
    rotate(polygon_.polygon_dots.begin(),
           std::min_element(polygon_.polygon_dots.begin(), polygon_.polygon_dots.end()),
           polygon_.polygon_dots.end());
}

polygon calculate_Minkowski_sum(polygon &first_polygon, polygon &second_polygon) {
    regroup_dots(first_polygon);
    regroup_dots(second_polygon);
    polygon Minkowski_sum;
    size_t first_polygon_inc = 0;
    size_t second_polygon_inc = 0;
    size_t first_size = first_polygon.size;
    size_t second_size = second_polygon.size;

    first_polygon.polygon_dots.push_back(first_polygon.polygon_dots[0]);
    second_polygon.polygon_dots.push_back(second_polygon.polygon_dots[0]);

    while (first_polygon_inc < first_size || second_polygon_inc < second_size) {
        Minkowski_sum.polygon_dots.push_back(
                first_polygon.polygon_dots[first_polygon_inc] + second_polygon.polygon_dots[second_polygon_inc]);

        double angle_1 = first_polygon.get_angle(first_polygon_inc);
        double angle_2 = second_polygon.get_angle(second_polygon_inc);

        if (angle_1 > angle_2 && second_polygon_inc < second_size) {
            ++second_polygon_inc;
        } else if (angle_1 < angle_2 && first_polygon_inc < first_size) {
            ++first_polygon_inc;
        } else {
            if (first_polygon_inc < first_size)
                ++first_polygon_inc;
            if (second_polygon_inc < second_size)
                ++second_polygon_inc;
        }
    }
    Minkowski_sum.size = Minkowski_sum.polygon_dots.size();
    return Minkowski_sum;
}

bool check_if_polygons_intersect(polygon &polygon_1, polygon &polygon_2) {
    polygon_2.reverse();
    polygon Minkowski_sum = calculate_Minkowski_sum(polygon_1, polygon_2);
    radius_vector_R2 null_point;
    return null_point.check_if_point_appertains_polygon(Minkowski_sum);
}
