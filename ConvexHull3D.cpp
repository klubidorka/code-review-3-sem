#include <array>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>
#include <set>
#include <queue>

struct radius_vector_R3 {
    double x;
    double y;
    double z;

    double modulus();

    radius_vector_R3(double x_, double y_, double z_);

    radius_vector_R3();

    radius_vector_R3 operator+(const radius_vector_R3 &second_dot);

    radius_vector_R3 operator-(const radius_vector_R3 &second_dot);

    radius_vector_R3 operator*(const double &multiplier);

};

std::istream &operator>>(std::istream &is, radius_vector_R3 &rad_vec);

struct plane {
    std::array<size_t, 3> points_indexes;

    plane(size_t first, size_t second, size_t third);
};

bool plane_cmp(const plane &p1, const plane &p2) {
    if (p1.points_indexes[0] < p2.points_indexes[0])
        return true;
    if (p1.points_indexes[0] > p2.points_indexes[0])
        return false;
    if (p1.points_indexes[1] < p2.points_indexes[1])
        return true;
    if (p1.points_indexes[1] > p2.points_indexes[1])
        return false;
    if (p1.points_indexes[2] < p2.points_indexes[2])
        return true;
    return (p1.points_indexes[2] <= p2.points_indexes[2]);
}

class convex_hull_3D {
public:
    void build_hull();

    friend std::istream &operator>>(std::istream &is, convex_hull_3D &hull);

    friend std::ostream &operator<<(std::ostream &os, convex_hull_3D &hull);

private:
    std::vector<radius_vector_R3> points;
    std::vector<plane> planes;
    size_t points_num;

    void find_first_plane();

    void find_other_planes();

    size_t find_second_point(const size_t first_point_index);

    radius_vector_R3 get_normal(const radius_vector_R3 &point_1,
                                const radius_vector_R3 &point_2,
                                const radius_vector_R3 &point_3);

    size_t find_third_point(const size_t first_point_index, const size_t second_point_index);

    radius_vector_R3 find_surface_normal(const size_t &first_point_index,
                                         const size_t &second_point_index,
                                         const size_t &third_point_index);

    void normalize_plane(plane &p);
};

std::istream &operator>>(std::istream &is, convex_hull_3D &hull);

std::ostream &operator<<(std::ostream &os, convex_hull_3D &hull);

// returns angle in range [0, pi/2]
double vector_horizontal_plane_angle(const radius_vector_R3 &dot) {
    return atan2(dot.z, hypot(fabs(dot.x), fabs(dot.y)));
}

radius_vector_R3 cross_product(radius_vector_R3 first, radius_vector_R3 second);

double inner_product(radius_vector_R3 first, radius_vector_R3 second);

int main() {
    int point_number;
    std::cin >> point_number;
    for (int i = 0; i < point_number; ++i) {
        convex_hull_3D hull;
        std::cin >> hull;
        hull.build_hull();
        std::cout << hull;
    }
    return 0;
}

double radius_vector_R3::modulus() {
    return sqrt(x * x + y * y + z * z);
}

void convex_hull_3D::find_first_plane() {
    double min_z = std::numeric_limits<double>::max();
    size_t point_with_min_z_index = 0;
    for (size_t i = 0; i < points_num; i++) {
        if (points[i].z < min_z) {
            min_z = points[i].z;
            point_with_min_z_index = i;
        }
    }
    size_t second_point_index = find_second_point(point_with_min_z_index);
    size_t third_point_index = find_third_point(point_with_min_z_index, second_point_index);
    plane new_plane(point_with_min_z_index, second_point_index, third_point_index);
    normalize_plane(new_plane);
    planes.push_back(new_plane);
}

void convex_hull_3D::find_other_planes() {
    std::set<std::pair<size_t, size_t>> current_intervals;
    std::queue<size_t> current_planes;

    current_planes.push(0);

    std::pair<size_t, size_t> first(planes[0].points_indexes[0], planes[0].points_indexes[1]);
    std::pair<size_t, size_t> second(planes[0].points_indexes[1], planes[0].points_indexes[2]);
    std::pair<size_t, size_t> third(planes[0].points_indexes[2], planes[0].points_indexes[0]);
    current_intervals.insert(first);
    current_intervals.insert(second);
    current_intervals.insert(third);

    while (!current_intervals.empty()) {

        size_t current_plane_index = current_planes.front();
        current_planes.pop();
        for (size_t i = 0; i < 3; ++i) {
            std::pair<size_t, size_t> current_interval(planes[current_plane_index].points_indexes[i],
                                                       planes[current_plane_index].points_indexes[(i + 1) % 3]);
            if (current_intervals.count(current_interval) > 0) {

                double maximal_inner_product = -1;
                size_t best_point_index = 0;
                radius_vector_R3 normal = find_surface_normal(current_interval.first,
                                                              current_interval.second,
                                                              planes[current_plane_index].points_indexes[(i + 2) % 3]);
                for (size_t item = 0; item < points_num; item++) {
                    if (item != current_interval.first &&
                        item != current_interval.second &&
                        item != planes[current_plane_index].points_indexes[(i + 2) % 3]) {

                        radius_vector_R3 pre_new_normal = cross_product(points[current_interval.second] - points[item],
                                                                        points[current_interval.first] -
                                                                        points[current_interval.second]);
                        radius_vector_R3 new_normal = pre_new_normal * (1 / pre_new_normal.modulus());


                        double new_inner_product = inner_product(normal, new_normal);
                        if (new_inner_product > maximal_inner_product) {
                            best_point_index = item;
                            maximal_inner_product = new_inner_product;
                        }

                    }
                }
                plane new_plane(current_interval.first,
                                current_interval.second,
                                best_point_index);
                normalize_plane(new_plane);

                for (size_t j = 0; j < 3; ++j) {
                    std::pair<size_t, size_t> new_front(new_plane.points_indexes[j],
                                                        new_plane.points_indexes[(j + 1) % 3]);
                    std::pair<size_t, size_t> new_back(new_plane.points_indexes[(j + 1) % 3],
                                                       new_plane.points_indexes[j]);
                    if (current_intervals.count(new_back) != 0) {
                        current_intervals.erase(new_back);
                    } else {
                        current_intervals.insert(new_front);
                    }
                }
                current_planes.push(planes.size());
                planes.push_back(new_plane);
            }
        }
    }
}

size_t convex_hull_3D::find_second_point(const size_t first_point_index) {
    double angle = M_PI;
    size_t best_point_index = 0;
    for (size_t item = 0; item < points_num; item++) {
        if (item != first_point_index) {
            double current_angle = vector_horizontal_plane_angle(points[item] - points[first_point_index]);
            if (current_angle < angle) {
                angle = current_angle;
                best_point_index = item;
            }
        }
    }
    return best_point_index;
}

radius_vector_R3
convex_hull_3D::get_normal(const radius_vector_R3 &point_1,
                           const radius_vector_R3 &point_2,
                           const radius_vector_R3 &point_3) {
    return {
            (point_2.y - point_1.y) * (point_3.z - point_1.z) -
            (point_3.y - point_1.y) * (point_2.z - point_1.z),
            (point_3.x - point_1.x) * (point_2.z - point_1.z) -
            (point_2.x - point_1.x) * (point_3.z - point_1.z),
            (point_2.x - point_1.x) * (point_3.y - point_1.y) -
            (point_3.x - point_1.x) * (point_2.y - point_1.y)
    };
}

size_t convex_hull_3D::find_third_point(const size_t first_point_index, const size_t second_point_index) {
    for (size_t item = 0; item < points_num; item++) {
        if (item != first_point_index && item != second_point_index) {
            bool item_is_ok = true;

            radius_vector_R3 point_1 = points[first_point_index];
            radius_vector_R3 point_2 = points[second_point_index];
            radius_vector_R3 point_3 = points[item];

            radius_vector_R3 surface_normal = get_normal(point_1, point_2, point_3);
            radius_vector_R3 back_surface_normal = surface_normal * (-1);

            radius_vector_R3 other_dot;

            for (size_t i = 0; i < points_num; i++) {
                if (i != first_point_index && i != second_point_index && i != item) {
                    other_dot = points[i];
                    break;
                }
            }
            radius_vector_R3 first_vector = points[first_point_index] + surface_normal - other_dot;

            radius_vector_R3 second_vector = points[first_point_index] - surface_normal - other_dot;

            if (first_vector.modulus() < second_vector.modulus()) {
                surface_normal = back_surface_normal;
            }
            for (int j = 0; j < points_num; ++j) {
                if (j != first_point_index && j != second_point_index && j != item) {
                    if ((points[first_point_index] + surface_normal - points[j]).modulus() <
                        (points[first_point_index] - surface_normal - points[j]).modulus()) {
                        item_is_ok = false;
                        break;
                    }
                }
            }
            if (item_is_ok) {
                return item;
            }
        }
    }
}

radius_vector_R3 convex_hull_3D::find_surface_normal(const size_t &first_point_index,
                                                     const size_t &second_point_index,
                                                     const size_t &third_point_index) {
    radius_vector_R3 point_1 = points[first_point_index];
    radius_vector_R3 point_2 = points[second_point_index];
    radius_vector_R3 point_3 = points[third_point_index];

    radius_vector_R3 surface_normal = get_normal(point_1, point_2, point_3);
    surface_normal = surface_normal * (1 / surface_normal.modulus());
    radius_vector_R3 back_surface_normal = surface_normal * (-1);
    radius_vector_R3 other_point;
    for (size_t i = 0; i < points_num; i++) {
        if (i != first_point_index && i != second_point_index && i != third_point_index) {
            other_point = points[i];
            break;
        }
    }
    radius_vector_R3 first_vector = surface_normal + points[first_point_index] - other_point;

    radius_vector_R3 second_vector = points[first_point_index] - surface_normal - other_point;

    if (first_vector.modulus() > second_vector.modulus()) {
        return surface_normal;
    }
    return back_surface_normal;
}

void convex_hull_3D::normalize_plane(plane &p) {
    std::rotate(p.points_indexes.begin(),
                std::min_element(p.points_indexes.begin(), p.points_indexes.end()),
                p.points_indexes.end());
    radius_vector_R3 normal = find_surface_normal(p.points_indexes[0], p.points_indexes[1], p.points_indexes[2]);
    radius_vector_R3 first = points[p.points_indexes[1]] - points[p.points_indexes[0]];
    radius_vector_R3 second = points[p.points_indexes[2]] - points[p.points_indexes[1]];

    radius_vector_R3 cross_product_ = cross_product(first, second);
    if (inner_product(normal, cross_product_) < 0) {
        std::swap(p.points_indexes[1], p.points_indexes[2]);
    }
}

radius_vector_R3::radius_vector_R3(double
                                   x_, double
                                   y_, double
                                   z_) : x(x_), y(y_), z(z_) {}

radius_vector_R3::radius_vector_R3() : x(0), y(0), z(0) {}

radius_vector_R3 radius_vector_R3::operator+(const radius_vector_R3 &second_dot) {
    return {x + second_dot.x,
            y + second_dot.y,
            z + second_dot.z};
}

radius_vector_R3 radius_vector_R3::operator-(const radius_vector_R3 &second_dot) {
    return {x - second_dot.x,
            y - second_dot.y,
            z - second_dot.z};
}

radius_vector_R3 radius_vector_R3::operator*(const double &multiplier) {
    return {x * multiplier,
            y * multiplier,
            z * multiplier};
}

std::istream &operator>>(std::istream &is, radius_vector_R3 &rad_vec) {
    is >> rad_vec.x;
    is >> rad_vec.y;
    is >> rad_vec.z;
    return is;
}

plane::plane(size_t
             first, size_t
             second, size_t
             third) {
    points_indexes[0] = first;
    points_indexes[1] = second;
    points_indexes[2] = third;
}

void convex_hull_3D::build_hull() {
    find_first_plane();
    find_other_planes();
}

std::istream &operator>>(std::istream &is, convex_hull_3D &hull) {
    size_t num;
    is >> num;
    radius_vector_R3 dot;
    hull.points_num = num;
    hull.points.reserve(num);
    for (size_t i = 0; i < num; ++i) {
        is >> dot;
        hull.points.push_back(dot);
    }
    return is;
}

std::ostream &operator<<(std::ostream &os, convex_hull_3D &hull) {
    std::sort(hull.planes.begin(), hull.planes.end(), plane_cmp);
    os << hull.planes.size() << std::endl;
    for (auto item : hull.planes) {
        os << "3 " << item.points_indexes[0] << " " << item.points_indexes[1] << " " << item.points_indexes[2]
           << std::endl;
    }
    return os;
}

radius_vector_R3 cross_product(radius_vector_R3 first, radius_vector_R3 second) {
    double det_i = first.y * second.z - first.z * second.y;
    double det_j = first.z * second.x - first.x * second.z;
    double det_k = first.x * second.y - first.y * second.x;
    return {det_i, det_j, det_k};
}

double inner_product(radius_vector_R3 first, radius_vector_R3 second) {
    return first.x * second.x + first.y * second.y + first.z * second.z;
}
