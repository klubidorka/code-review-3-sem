﻿
// Алгоритм Кнута - Морриса - Пратта поиска всех вхождений шаблона в строку.
// Время O(n + p), доп. память – O(p), где p - длина шаблона, n - длина текста.

// https://contest.yandex.ru/contest/9093/run-report/11948592/

#include <iostream>
#include <string>
#include <vector>

// Расчет префикс-функции для шаблона
std::vector<unsigned int> calculate_prefix_function(const std::string &pattern) {
    std::vector<unsigned int> prefix_function(pattern.size());

    // Значение префикс-функции в рассматриваемой точке
    if (!pattern.empty()) {
        prefix_function[0] = 0;
    }

    unsigned int match_len = 0;

    for (unsigned int j = 1; j < pattern.size(); j++) {
        while (match_len > 0 && pattern[j] != pattern[match_len])
            match_len = prefix_function[match_len - 1];
        if (pattern[match_len] == pattern[j]) {
            prefix_function[j] = ++match_len;
        } else {
            prefix_function[j] = 0;
        }
    }

    return prefix_function;
}

std::vector<unsigned int> substring_search(const std::string &pattern) {
    std::vector<unsigned int> pattern_prefix_function = calculate_prefix_function(pattern);

    std::vector<unsigned int> inclusion_indices;

    // Значение префикс-функции в рассматриваемой точке
    unsigned int match_len = 0;
    unsigned int received_text_len = 1;

    // Случай пустого шаблона вырожденный и требует другого значения переменной для корректной работы
    // алгоритма
    if (pattern.empty()){
        received_text_len = 0;
    }
    char current_letter;

    // Поточный расчет префикс-функции для текста
    while (std::cin >> current_letter) {
        while (match_len > 0 && current_letter != pattern[match_len]) {
            match_len = pattern_prefix_function[match_len - 1];
        }
        if (pattern[match_len] == current_letter) {
            ++match_len;
        }
        if (match_len == pattern.size()) {
            inclusion_indices.push_back(received_text_len - pattern.size());
        }
        ++received_text_len;
    }

    return inclusion_indices;
}


int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::string pattern;
    std::cin >> pattern;
    std::vector<unsigned int> answers = substring_search(pattern);

    for (unsigned int i : answers) {
        std::cout << i << " ";
    }
    std::cout << std::endl;
    return 0;
}
