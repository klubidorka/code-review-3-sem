#include <array>
#include <cassert>
#include <cmath>
#include <iomanip>
#include <iostream>

struct Radius_vector_R3 {
    double x;
    double y;
    double z;

    Radius_vector_R3(double x_, double y_, double z_);

    Radius_vector_R3();

    const double modulus() const;

    const Radius_vector_R3 operator+(const Radius_vector_R3 &second_point);

    const Radius_vector_R3 operator-(const Radius_vector_R3 &second_point);

    const Radius_vector_R3 operator/(const double &divider);

    const Radius_vector_R3 operator*(const double &multiplier);

    friend const Radius_vector_R3 operator+(const Radius_vector_R3 &first_point, const Radius_vector_R3 &second_point);

    friend const Radius_vector_R3 operator-(const Radius_vector_R3 &first_point, const Radius_vector_R3 &second_point);

    friend const Radius_vector_R3 operator/(const Radius_vector_R3 &point, const double &divider);

    friend const Radius_vector_R3 operator*(const Radius_vector_R3 &point, const double &multiplier);

    friend std::istream &operator>>(std::istream &is, Radius_vector_R3 &point);
};

struct Segment {
    Radius_vector_R3 first_point;
    Radius_vector_R3 second_point;

    Segment();

    friend std::istream &operator>>(std::istream &is, Segment &segment);

    Radius_vector_R3 get(double alpha) {
        return first_point + (second_point - first_point) * alpha;
        //return second_point + (first_point - second_point) * alpha;
    }
};

const double point_point_distance(Radius_vector_R3 first_point, Radius_vector_R3 second_point);

double segment_segment_distance(Segment segment_1, Segment segment_2);

int main() {
    std::cout << std::fixed << std::setprecision(8);

    Segment first_segment;
    Segment second_segment;
    std::cin >> first_segment >> second_segment;

    double distance = segment_segment_distance(first_segment, second_segment);
    std::cout << distance << std::endl;
    return 0;
}

Radius_vector_R3::Radius_vector_R3(double x_, double y_, double z_) : x(x_), y(y_), z(z_) {}

Radius_vector_R3::Radius_vector_R3() : x(0.0), y(0.0), z(0.0) {}

const double Radius_vector_R3::modulus() const {
    return sqrt(x * x + y * y + z * z);
}

const Radius_vector_R3 Radius_vector_R3::operator+(const Radius_vector_R3 &second_point) {
    return {x + second_point.x,
            y + second_point.y,
            z + second_point.z};
}

const Radius_vector_R3 operator+(const Radius_vector_R3 &first_point, const Radius_vector_R3 &second_point) {
    return {first_point.x + second_point.x,
            first_point.y + second_point.y,
            first_point.z + second_point.z};
}

const Radius_vector_R3 Radius_vector_R3::operator-(const Radius_vector_R3 &second_point) {
    return {x - second_point.x,
            y - second_point.y,
            z - second_point.z};
}

const Radius_vector_R3 operator-(const Radius_vector_R3 &first_point, const Radius_vector_R3 &second_point) {
    return {first_point.x - second_point.x,
            first_point.y - second_point.y,
            first_point.z - second_point.z};
}

const Radius_vector_R3 Radius_vector_R3::operator/(const double &divider) {
    assert(divider != 0);
    return {
            x /= divider,
            y /= divider,
            z /= divider
    };
}

const Radius_vector_R3 operator/(const Radius_vector_R3 &point, const double &divider) {
    assert(divider != 0);
    return {
            point.x / divider,
            point.y / divider,
            point.z / divider
    };
}

const Radius_vector_R3 Radius_vector_R3::operator*(const double &multiplier) {
    return {
            x * multiplier,
            y * multiplier,
            z * multiplier
    };
}

const Radius_vector_R3 operator*(const Radius_vector_R3 &point, const double &multiplier) {
    return {
            point.x * multiplier,
            point.y * multiplier,
            point.z * multiplier
    };
}

std::istream &operator>>(std::istream &is, Radius_vector_R3 &point) {
    double x, y, z;
    is >> x >> y >> z;
    point.x = x;
    point.y = y;
    point.z = z;
    return is;
}

std::istream &operator>>(std::istream &is, Segment &segment) {
    is >> segment.first_point >> segment.second_point;
    return is;
}

Segment::Segment() {
    first_point = Radius_vector_R3();
    second_point = Radius_vector_R3();
}

const double point_point_distance(Radius_vector_R3 first_point, Radius_vector_R3 second_point) {
    return (first_point - second_point).modulus();
}

template<typename T, typename F>
static T ternary_search(F func, T left, T right, size_t iter_count) {
    T mid_1 = left + (right - left) / 3;
    T mid_2 = right - (right - left) / 3;
    size_t it_index = 0;
    while (right - left > 1e-10 && it_index < iter_count) {
        if (func(mid_1) > func(mid_2)) {
            left = mid_1;
        } else {
            right = mid_2;
        }
        mid_1 = left + (right - left) / 3;
        mid_2 = right - (right - left) / 3;
        it_index++;
    }
    return func((mid_2 + mid_1) / 2);
}

double segment_segment_distance(Segment segment_1, Segment segment_2) {
    const size_t num_of_iterations = 1000;
    auto f = [&segment_1, &segment_2](const double &first_move) -> double {
        auto g = [&segment_1, &segment_2, &first_move](const double &second_move) -> double {
            return point_point_distance(segment_1.get(first_move),
                                        segment_2.get(second_move));
        };
        return ternary_search(g, 0.0, 1.0, num_of_iterations);
    };
    return ternary_search(f, 0.0, 1.0, num_of_iterations);
}
